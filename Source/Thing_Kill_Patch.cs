﻿using HarmonyLib;
using RimWorld;
using VanillaBooksExpanded;
using Verse;

namespace VanillaBooksExpandedExpanded
{
    [HarmonyPatch(typeof(Thing), nameof(Thing.Kill))]
    static class Thing_Kill_Patch
    {
        public static void Prefix(Thing __instance, DamageInfo? dinfo)
        {
            if (__instance is Book && !(__instance is Newspaper))
            {
                var instigator = dinfo?.Instigator;

                var eventDef = VBEE_DefOf.VanillaBooksExpandedExpanded_BookKilled;
                if (eventDef != null)
                {
                    Find.HistoryEventsManager.RecordEvent(new HistoryEvent(
                        eventDef,
                        instigator.Named(HistoryEventArgsNames.Doer),
                        __instance.Named(HistoryEventArgsNames.Victim)
                    ));
                }
            }
        }
    }
}
