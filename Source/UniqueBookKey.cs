﻿using System;
using VanillaBooksExpanded;
using Verse;

namespace VanillaBooksExpandedExpanded
{
    public class UniqueBookKey : IEquatable<UniqueBookKey>
    {
        public UniqueBookKey(ThingDef def, TaleBookReference taleRef)
        {
            this.Def = def;
            this.TaleRef = taleRef;
        }

        public ThingDef Def { get; }
        public TaleBookReference TaleRef { get; }

        public static UniqueBookKey For(Thing book)
        {
            if (book == null)
            {
                return null;
            }

            var comp = book.TryGetComp<CompBook>();
            if (comp == null || !comp.Active)
            {
                return null;
            }

            if (book is IdeoligionBook ideo)
            {
                return new UniqueBookKey_Ideoligion(book.def, comp.TaleRef, ideo.ideoligion);
            }

            return new UniqueBookKey(book.def, comp.TaleRef);
        }

        public static bool operator ==(UniqueBookKey a, UniqueBookKey b)
        {
            if (a is null)
            {
                return b is null;
            }

            return a.Equals(b);
        }

        public static bool operator !=(UniqueBookKey a, UniqueBookKey b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (obj is UniqueBookKey key)
            {
                return Equals(key);
            }

            return false;
        }

        public override int GetHashCode()
        {
            var hash = this.GetType().GetHashCode();
            hash = Gen.HashCombine(hash, this.Def);
            hash = Gen.HashCombine(hash, this.TaleRef.tale);
            hash = Gen.HashCombine(hash, this.TaleRef.seed);
            return hash;
        }

        public virtual bool Equals(UniqueBookKey key)
        {
            return !(key is null) &&
                key.GetType() == this.GetType() &&
                key.Def == this.Def &&
                key.TaleRef.tale == this.TaleRef.tale &&
                key.TaleRef.seed == this.TaleRef.seed;
        }
    }
}
