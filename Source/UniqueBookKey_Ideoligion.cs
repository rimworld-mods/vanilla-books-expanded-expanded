﻿using RimWorld;
using VanillaBooksExpanded;
using Verse;

namespace VanillaBooksExpandedExpanded
{
    public class UniqueBookKey_Ideoligion : UniqueBookKey
    {
        public Ideo Ideo { get; }

        public UniqueBookKey_Ideoligion(ThingDef def, TaleBookReference taleRef, Ideo ideo) : base(def, taleRef)
        {
            this.Ideo = ideo;
        }

        public override int GetHashCode()
        {
            return Gen.HashCombine(base.GetHashCode(), this.Ideo);
        }

        public override bool Equals(UniqueBookKey key)
        {
            return base.Equals(key) && key is UniqueBookKey_Ideoligion ideo && ideo.Ideo == this.Ideo;
        }
    }
}