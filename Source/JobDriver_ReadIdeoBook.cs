﻿using System.Collections.Generic;
using System.Reflection;
using HarmonyLib;
using RimWorld;
using UnityEngine;
using VanillaBooksExpanded;
using Verse;
using Verse.AI;

namespace VanillaBooksExpandedExpanded
{
    [StaticConstructorOnStartup]
    public class JobDriver_ReadIdeoBook : JobDriver_ReadBook
    {
        private static readonly FieldInfo CertaintyField = AccessTools.Field(typeof(Pawn_IdeoTracker), "certainty");
        private static readonly FieldInfo CurReadingTicksField = AccessTools.Field(typeof(JobDriver_ReadBook), "curReadingTicks");
        private static readonly MethodInfo FindSeatsForReadingMethod = AccessTools.Method(typeof(JobDriver_ReadBook), "FindSeatsForReading");
        private static readonly PropertyInfo TotalReadingTicksProperty = AccessTools.Property(typeof(JobDriver_ReadBook), "totalReadingTicks");

        protected IdeoligionBook book => this.TargetThingB as IdeoligionBook;

        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnDestroyedOrNull(TargetIndex.B);
            yield return Toils_Reserve.Reserve(TargetIndex.B);
            yield return Toils_Goto.GotoThing(TargetIndex.B, PathEndMode.Touch);
            pawn.CurJob.count = 1;
            yield return Toils_Haul.StartCarryThing(TargetIndex.B);
            yield return ((Toil)FindSeatsForReadingMethod.Invoke(this, new object[] { pawn })).FailOnForbidden(TargetIndex.C);

            var toil = new Toil();
            toil.AddPreInitAction(delegate
            {
                pawn.CurJob.targetA = pawn;
                if (pawn.carryTracker.CarriedThing is Book)
                {
                    book.stopDraw = true;
                }

                pawn.GainComfortFromCellIfPossible();
                if (book.Props.saveReadingProgress)
                {
                    CurReadingTicksField.SetValue(this, book.curReadingTicks);
                }
            });
            toil.tickAction = delegate
            {
                Pawn pawn = base.pawn;
                if (base.TargetC.HasThing)
                {
                    pawn.Rotation = base.TargetC.Thing.Rotation;
                }

                var ideoBook = this.book;
                if (ideoBook != null && pawn?.ideo != null)
                {
                    // can't call OffsetCertainty or it'll spam text motes
                    float modified = Mathf.Clamp01(pawn.ideo.Certainty + ideoBook.GetCertaintyAmountPerTick(pawn));
                    CertaintyField.SetValue(pawn.ideo, modified);
                }

                if (book.Props.joyAmountPerTick > 0f)
                {
                    base.pawn.needs.joy.GainJoy(book.Props.joyAmountPerTick, VBE_DefOf.VBE_Reading);
                }

                var readingTicks = (int)CurReadingTicksField.GetValue(this);
                readingTicks++;
                CurReadingTicksField.SetValue(this, readingTicks);
                book.curReadingTicks = readingTicks;
                if (readingTicks > (int)TotalReadingTicksProperty.GetValue(this))
                {
                    if (base.pawn.carryTracker.CarriedThing is Book)
                    {
                        book.stopDraw = false;
                    }

                    ReadyForNextToil();
                }
            };
            toil.handlingFacing = true;
            toil.WithEffect(() => book.Props.readingEffecter, () => base.TargetA);
            toil.defaultCompleteMode = ToilCompleteMode.Never;
            toil.WithProgressBar(TargetIndex.B, () => (float)(int)CurReadingTicksField.GetValue(this) / (float)(int)TotalReadingTicksProperty.GetValue(this));
            yield return toil;
            yield return new Toil
            {
                initAction = delegate
                {
                    if (pawn.carryTracker.CarriedThing is Book)
                    {
                        this.book.stopDraw = false;
                    }

                    if (pawn.GetRoom()?.Role == VBE_DefOf.VBE_Library)
                    {
                        TryGainLibraryRoomThought(pawn);
                    }
                    else
                    {
                        JoyUtility.TryGainRecRoomThought(pawn);
                    }

                    if (VBEE_DefOf.VME_ReadBook != null)
                    {
                        Find.HistoryEventsManager.RecordEvent(new HistoryEvent(VBEE_DefOf.VME_ReadBook, pawn.Named(HistoryEventArgsNames.Doer)));
                    }

                    if (this.book.Props.destroyAfterReading)
                    {
                        this.book.Destroy();
                    }
                    else
                    {
                        Thing book = this.book;
                        StoragePriority currentPriority = StoreUtility.CurrentStoragePriorityOf(book);
                        if (StoreUtility.TryFindBestBetterStoreCellFor(book, pawn, base.Map, currentPriority, pawn.Faction, out IntVec3 foundCell))
                        {
                            job.SetTarget(TargetIndex.C, foundCell);
                            job.SetTarget(TargetIndex.B, book);
                            job.count = book.stackCount;
                            return;
                        }
                    }

                    EndJobWith(JobCondition.Succeeded);
                },
                defaultCompleteMode = ToilCompleteMode.Instant
            };
        }
    }
}
